/* AUTOMATICALLY GENERATED VERILOG-2001 SOURCE CODE.
** GENERATED BY CLASH 0.99.3. DO NOT MODIFY.
*/
module VGA_bram
    ( // Inputs
      input  \$dIP  // clock
    , input [16:0] rd 

      // Outputs
    , output wire [11:0] result 
    );
  wire [3:0] a3;
  wire [3:0] a1;
  wire [3:0] b1;
  wire [7:0] ds1;
  wire [11:0] ds4;
  reg [11:0] x;
  wire [7:0] b;
  wire signed [63:0] wild;
  wire signed [63:0] \#wild_app_arg ;
  wire signed [63:0] x_res;
  wire signed [63:0] x_res_res;

  assign result = {a3,a1,b1};

  assign a3 = ds4[11:8];

  assign a1 = ds1[7:4];

  assign b1 = ds1[3:0];

  assign ds1 = b;

  assign ds4 = x;

  assign x_res_res = $unsigned(({17 {1'bx}}));

  assign x_res = $signed(x_res_res);

  // blockRamFile begin
  reg [11:0] RAM [0:76800-1];

  initial begin
    $readmemb("marisa.txt",RAM);
  end

  always @(posedge \$dIP ) begin : VGA_bram_blockRamFile
    if (1'b0) begin
      RAM[(x_res)] <= ({12 {1'bx}});
    end
    x <= RAM[(wild)];
  end
  // blockRamFile end

  assign b = ds4[7:0];

  assign wild = $signed(\#wild_app_arg );

  assign \#wild_app_arg  = $unsigned(rd);
endmodule

