# clash-fb_vga

Simple Clash design that reads from a frame buffer and outputs VGA signals (640x480 @ 60 Hz). It is written for the Digilent VGA Pmod which uses 4 bits per pixel.

## How to use

1. Install Clash 0.99.3
2. Start clashi
3. Type `:l VGA` to load the design.
4. Type `:verilog` to generate verilog code. You can also use `:vhdl` and `:systemverilog` but apparently the initializing BRAM with a file doesn't work when generating VHDL.

The BRAM is initialized with a file that stores 12 bit binary strings per line. The sample image `marisa.txt` is a 320x240 image of Kirisame Marisa ([orignal](https://www.google.com/imgres?imgurl=https%3A%2F%2Fi.ytimg.com%2Fvi%2FEXdZGUsavAo%2Fmaxresdefault.jpg&imgrefurl=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DEXdZGUsavAo&docid=QTePxwkfG2XdiM&tbnid=JusBJ5ooAr2-WM%3A&vet=12ahUKEwiI6srH4L7fAhUBfFAKHT_DDOg4ZBAzKAkwCXoECAEQCg..i&w=1280&h=720&bih=961&biw=1920&q=love%20colored%20master%20spark&ved=2ahUKEwiI6srH4L7fAhUBfFAKHT_DDOg4ZBAzKAkwCXoECAEQCg&iact=mrc&uact=8)).
The 12 bits per line are stored in BGR444 format.

If you are using the pre-generated Verilog files in the repository, the copy marisa.txt to the `verilog/VGA/fb_vga/` folder. I didn't include it since it's fairly large and it's already in the root of 
the repository.

## The result

[Twitter](https://twitter.com/BitlogIT/status/1078069375679254529)
![Image](https://pbs.twimg.com/media/DvYR-noWoAAjv7J.jpg:large)
