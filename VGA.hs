module VGA where

import Clash.Prelude

-- Handy type definitions
type FBIndex = Unsigned 17
type Red     = BitVector 4
type Green   = BitVector 4
type Blue    = BitVector 4
type Color   = (Blue, Green, Red)
type HSync   = Bit
type VSync   = Bit
type HCntr   = Unsigned 10
type VCntr   = Unsigned 10
type VGAcomb = (HSync, VSync, Red, Green, Blue, HCntr, VCntr, FBIndex)

-- Stores 320 * 240 = 76800 pixels.
-- Each pixel is 12 bits stored in RGB444 format.
bram
  :: HiddenClock domain gated
  => Signal domain FBIndex
  -> Signal domain (Blue, Green, Red)
bram rd = unpack <$> blockRamFile (SNat @76800) "marisa.txt" rd (pure Nothing)

-- Generates VGA signals for 640x480 @ 60 Hz.
-- Input is 4 bpp in BGR order.
-- Also outputs a memory address that is used to fetch pixels from a frame buffer.
vgaComb
  :: VGAcomb
  -> Color
  -> (VGAcomb, (HSync, VSync, Red, Green, Blue, FBIndex))
vgaComb (hsync, vsync, red, green, blue, hcnt, vcnt, addr) (b, g, r) = ((hsync', vsync', red', green', blue', hcnt', vcnt', addr'), (hsync', vsync', red', green', blue', addr'))
  where
    -- The values for the counters, hsync, and vsync can be found at:
    -- http://tinyvga.com/vga-timing/640x480@60Hz
    hcnt' | hcnt == 799 = 0
          | otherwise   = hcnt + 1
    
    vcnt' | hcnt == 799 && vcnt == 524 = 0
          | hcnt == 799                = vcnt + 1
          | otherwise                  = vcnt

    hsync' | hcnt >= 656 && hcnt <= 751 = 1
           | otherwise                  = 0

    vsync' | vcnt >= 490 && vcnt <= 491 = 1
           | otherwise                  = 0

    -- Determines when we are allowed to draw
    draw = hcnt < 640 && vcnt < 480

    -- We repeat the same address so we get pixel doubling.
    -- There is not enough BRAM on the used FPGA so we use this to fill the screen.
    addr' = case draw of
      True -> ((extend $ vcnt `shiftR` 1) * (320 :: Unsigned 17) + (extend $ hcnt `shiftR` 1))
      _    -> 0

    -- We have to set the color channels to zero when we are not in the drawing region.
    -- Outside the drawing region, the color channel levels are used to calibrate what
    -- the levels for black are.
    red' | draw      = r
         | otherwise = 0

    green' | draw      = g
           | otherwise = 0

    blue' | draw      = b
          | otherwise = 0

-- Clocked version of vgaComb
vga = mealy vgaComb (0, 0, 0, 0, 0, 0, 0, 0)

-- Connects the BRAM and VGA generator together
fb_vga
  :: HiddenClockReset domain Source Synchronous
  => Signal domain (HSync, VSync, Red, Green, Blue)
fb_vga = o
  where
    colordata = bram fb_addr
    (hsync, vsync, red, green, blue, fb_addr) = unbundle (vga colordata)
    o = bundle (hsync, vsync, red, green, blue)

{-# ANN topEntity
  (Synthesize
    { t_name   = "fb_vga"
    , t_inputs = [PortName "clk", PortName "rst"]
    , t_output = PortProduct "" [PortName "hsync", PortName "vsync", PortName "r", PortName "g", PortName "b"] }
  ) #-}
topEntity
  :: Clock System Source
  -> Reset System Synchronous
  -> Signal System (HSync, VSync, Red, Green, Blue)
topEntity clk rst = exposeClockReset fb_vga clk rst
